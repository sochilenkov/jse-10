package ru.t1.sochilenkov.tm.service;

import ru.t1.sochilenkov.tm.api.ICommandRepository;
import ru.t1.sochilenkov.tm.api.ICommandService;
import ru.t1.sochilenkov.tm.model.Command;

public final class CommandService implements ICommandService {

    private ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    public Command[] getCommands() {
        return commandRepository.getCommands();
    }

}
