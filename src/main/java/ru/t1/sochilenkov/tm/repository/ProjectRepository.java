package ru.t1.sochilenkov.tm.repository;

import ru.t1.sochilenkov.tm.model.Project;
import ru.t1.sochilenkov.tm.api.IProjectRepository;


import java.util.ArrayList;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    @Override
    public List<Project> findAll() {
        return projects;
    }

    @Override
    public Project add(Project project) {
        projects.add(project);
        return project;
    }

    @Override
    public void clear() {
        projects.clear();
    }

}
