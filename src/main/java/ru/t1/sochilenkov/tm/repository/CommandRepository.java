package ru.t1.sochilenkov.tm.repository;

import ru.t1.sochilenkov.tm.api.ICommandRepository;
import ru.t1.sochilenkov.tm.constant.ArgumentConstant;
import ru.t1.sochilenkov.tm.constant.CommandConstant;
import ru.t1.sochilenkov.tm.model.Command;

public final class CommandRepository implements ICommandRepository {

    public static final Command HELP = new Command(CommandConstant.HELP, ArgumentConstant.HELP, "Show help info");

    public static final Command VERSION = new Command(CommandConstant.VERSION, ArgumentConstant.VERSION, "Show version info");

    public static final Command ABOUT = new Command(CommandConstant.ABOUT, ArgumentConstant.ABOUT, "Show developer info");

    public static final Command INFO = new Command(CommandConstant.INFO, ArgumentConstant.INFO, "Show system info");

    public static final Command PROJECT_LIST = new Command(CommandConstant.PROJECT_LIST, null, "Show project list");

    public static final Command PROJECT_CREATE = new Command(CommandConstant.PROJECT_CREATE, null, "Create new project");

    public static final Command PROJECT_CLEAR = new Command(CommandConstant.PROJECT_CLEAR, null, "Delete all projects");

    public static final Command TASK_LIST = new Command(CommandConstant.TASK_LIST, null, "Show task list");

    public static final Command TASK_CREATE = new Command(CommandConstant.TASK_CREATE, null, "Create new task");

    public static final Command TASK_CLEAR = new Command(CommandConstant.TASK_CLEAR, null, "Delete all tasks");

    public static final Command EXIT = new Command(CommandConstant.EXIT, null, "Close application");

    public static final Command[] COMMANDS = new Command[]{
            HELP, VERSION, ABOUT, INFO, EXIT, PROJECT_CREATE,
            PROJECT_LIST, PROJECT_CLEAR, TASK_CREATE, TASK_LIST, TASK_CLEAR
    };

    public Command[] getCommands() {
        return COMMANDS;
    }

}
