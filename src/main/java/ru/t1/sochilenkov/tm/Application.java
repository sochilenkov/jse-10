package ru.t1.sochilenkov.tm;

import ru.t1.sochilenkov.tm.api.ICommandController;
import ru.t1.sochilenkov.tm.api.ICommandRepository;
import ru.t1.sochilenkov.tm.api.ICommandService;
import ru.t1.sochilenkov.tm.component.Bootstrap;
import ru.t1.sochilenkov.tm.constant.ArgumentConstant;
import ru.t1.sochilenkov.tm.constant.CommandConstant;
import ru.t1.sochilenkov.tm.controller.CommandController;
import ru.t1.sochilenkov.tm.model.Command;
import ru.t1.sochilenkov.tm.repository.CommandRepository;
import ru.t1.sochilenkov.tm.service.CommandService;
import ru.t1.sochilenkov.tm.util.FormatUtil;
import ru.t1.sochilenkov.tm.util.TerminalUtil;

import static ru.t1.sochilenkov.tm.util.FormatUtil.formatBytes;

public final class Application {

    public static void main(String[] args) {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}