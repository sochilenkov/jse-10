package ru.t1.sochilenkov.tm.api;

import ru.t1.sochilenkov.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    List<Project> findAll();

    Project add(Project project);

    void clear();

}
