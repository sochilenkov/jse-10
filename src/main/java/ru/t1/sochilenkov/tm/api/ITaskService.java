package ru.t1.sochilenkov.tm.api;

import ru.t1.sochilenkov.tm.model.Task;

public interface ITaskService extends ITaskRepository {

    Task create(String name, String description);

}
