package ru.t1.sochilenkov.tm.api;

import ru.t1.sochilenkov.tm.model.Project;

public interface IProjectService extends IProjectRepository {

    Project create(String name, String description);

}
