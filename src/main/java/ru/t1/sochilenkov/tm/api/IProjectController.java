package ru.t1.sochilenkov.tm.api;

public interface IProjectController {

    void showProjects();

    void createProject();

    void clearProjects();

}
