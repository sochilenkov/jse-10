package ru.t1.sochilenkov.tm.api;

public interface ICommandController {

    void showErrorArgument();

    void showErrorCommand();

    void showSystemInfo();

    void showVersion();

    void showAbout();

    void showHelp();

}
