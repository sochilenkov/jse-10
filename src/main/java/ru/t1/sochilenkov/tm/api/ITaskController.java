package ru.t1.sochilenkov.tm.api;

public interface ITaskController {

    void showTasks();

    void createTask();

    void clearTasks();

}
